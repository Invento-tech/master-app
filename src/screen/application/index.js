import React from 'react';
import ApplicationForm from "@compound/applicationForm";
import Footer from "@compound/footer";
import FlexView from "@atom/FlexView";

export default () => <FlexView>
    <ApplicationForm/>
    <Footer/>
  </FlexView>