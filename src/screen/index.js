import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import Home from "@screen/home";
import Application from "@screen/application";

export default () => (
    <Switch>
      <Route exact path="/" component={Home} />
      <Route exact path="/application-form" component={Application} />
    </Switch>
);
