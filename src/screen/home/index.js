import React from 'react';
import { observer, inject } from "mobx-react";
import FlexView, {FlexViewImage} from '@atom/FlexView';
import { styled } from '@material-ui/styles';
import Input from "@atom/Input";
import { EnhancedText as Text} from "@atom/Text";
import Footer from "@compound/footer";
import bannerImage from "@asset/home-banner.svg"
import ApplicationBanner from "@molecule/applicationBanner";
import Article from "@compound/article";
import OurClient from '@compound/ourClient';
import Specialities from '@compound/specialities';
import OurService from "@compound/ourService";
import ContactUs from "@compound/contactUs";
import { theme } from "@core/themes";
import TitleBorder from "@atom/Border";

export default () => 
<FlexView width={"100%"} background={theme.common.white} margin={"auto"} >
    <FlexViewImage src={bannerImage} height={'calc(100vh + 120px)'} width={"100%"} justify={"center"} align={"center"} flex={"none"}>
        <FlexView>
            <Text className={"home-title centered"} size={theme.font.size.header} weight={"bold"}>{"MASTER TECHNOLOGIES"}</Text>
            <FlexView maxWidth={780} className={"padded-view"} padding={35}>
                <Text className={"non-padded-details centered"} size={theme.font.size.bigDetails} weight={"bold"}>{"IT/ICT Master Recruitment Hub"}</Text>
                <Text className={"non-padded-details"} align={"center"} size={theme.font.size.bigDetails} weight={"bold"}>{"If you're looking for a job, or even if you are happy with your current job, no matter what the position you are holding now - WE WANT YOU!"}</Text>
            </FlexView>
        </FlexView>
    </FlexViewImage>
    <FlexView height={'100%'} width={"100%"} justify={"flex-start"}>
        <FlexView 
            background={theme.background.normal}
            width={"100%"}
            padding={"60px 0"}
        >
            <FlexView direction={"row"} wrap={"wrap"} padding={theme.font.padding.title}>
                <Text className={"title"} size={theme.font.size.title} padding={"0 15px 0 0"} primary>{"WHY"}</Text>
                <Text className={"title"} size={theme.font.size.title}>{"Master Technologies?"}</Text>
            </FlexView>
            <TitleBorder/>
            <FlexView maxWidth={998} className={"padded-view"} padding={20}>
                <Text className={"details"} size={theme.font.size.details} padding={20} align={"center"} weight={"lighter"}>{"Master Technologies (MT) will be the first IT/ICT hub for all IT professionals from Fresh Graduates, Specialist, Engineer, Developer, LEAD, Manager or even CTO/CIO positions. It is the right website to register and be a part of."}</Text>
            </FlexView>
        </FlexView>
        <Article/>
    </FlexView>
    <FlexView height={"100%"} width={"100%"} className={"page-break"}>
        <ApplicationBanner width={"100%"}/>
        <Specialities/>
        {/* <OurClient/> */}
    </FlexView>

    {/* <OurService/> */}
    <ContactUs/>
    <Footer/>
</FlexView>