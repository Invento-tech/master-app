import React, {useState} from "react";
import FlexView from "@atom/FlexView";
import Text, { EnhancedText } from "@atom/Text";
import { styled } from "@material-ui/styles";
import { theme } from "@core/themes";

const Label = styled("label")(({theme, htmlFor, size, color, family})=>({
  fontSize: size || theme.font.size.details,
  color: color || theme.font.color.gray,
  fontFamily: family || theme.font.family,
  htmlFor,
  cursor: "pointer",
  padding: "12px 37px",
  background: theme.common.white,
  borderRadius: 19,
  whiteSpace: "nowrap"
}))

export default () => {
  const [path, setPath] = useState("");
  return <FlexView width={"100%"} align={"flex-start"}>
      <FlexView direction={"row"} wrap={"wrap"} align={"flex-start"} width={"100%"}>
        <Label size={theme.font.size.details} htmlFor="file-upload" className="details custom-file-upload">
          {"Upload File"}
        </Label>
        {
          path === "" ?
          <Text minWidth={300} flex={1} className={"details centered"} weight={"lighter"} width={"100%"} padding={22}>{"No file choosen"}</Text>
          :  
          <Text minWidth={300} width={"100%"} flex={1} padding={22}>{path}</Text>
        }
        <input style={{display: "none"}} onChange={(e)=>setPath(e.target.value)} id="file-upload" type="file"
          accept=".doc, .docx, .rtf, .pdf, application/msword, application/vnd.openxmlformats-officedocument.wordprocessingml.document, application/rtf, application/pdf"
        />
      </FlexView>
      <Text color={theme.font.color.gray} className={"centered"} size={13} weight={"lighter"}>{"DOC, DOCX, RTF, PDF-300KB MAX PREFERRED CV FORMAT - DOCX FILE"}</Text>
    </FlexView>
}