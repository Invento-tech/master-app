import React from "react";
import FlexView, { FlexViewImage } from "@atom/FlexView";
import Image from "@atom/Image";
import Text, { EnhancedText } from "@atom/Text";
import { styled } from "@material-ui/styles";
import applicationBanner from "@asset/fill-up-banner.svg";
import { history } from "@core/Router";
import { theme } from "@core/themes";
import Button from "@molecule/formButton";

export default ({}) => (
  <FlexViewImage src={applicationBanner} size={"auto"} height={200} width={"100%"} direction={"row"} justify={"space-evenly"} wrap={"wrap"}>
    <FlexView className={"padded-view wrapped-text"} justify={"space-evenly"} flex={4} wrap={"wrap"} direction={"row"}>
      <EnhancedText primary className={"title"} size={theme.font.size.title}>{"INTERESTED?"}</EnhancedText>
      <EnhancedText size={theme.font.size.details} className={"non-padded-details"}>{"Join us by filling up the application form!"}</EnhancedText>
    </FlexView>
    <FlexView className={"padded-view"} minWidth={223} padding={"20px"}>
      <Button label={"Apply Now"} onClick={()=>history.push("/application-form")} weight={"bold"}/>
    </FlexView>
  </FlexViewImage>
)