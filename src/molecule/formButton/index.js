import React from "react";
import BaseButton from "@atom/Button";
import Text from "@atom/Text";
import { styled } from "@material-ui/styles";
import { theme } from "@core/themes";

const ButtonText = styled(({disabled, cancel, children, ...rest})=><Text {...rest}>{children}</Text>)(({theme, disabled, cancel})=>({
  color: disabled ? theme.font.color.white : cancel ? theme.common.primary : theme.font.color.dark,
  fontSize: theme.font.size.buttonText,
}))

const Button = styled(({cancel, ...rest})=><BaseButton {...rest} />)(({theme, cancel})=>({
  border: cancel ? `1px solid ${theme.common.primary}` : "none"
}))

export default ({label, weight, onClick, disabled, background, cancel, ...rest}) => {
  return <Button cancel={cancel} background={disabled ? theme.common.gray : cancel ? theme.common.dark : background} width={180} height={60} radius={75} onClick={disabled ? ()=>{return null} : ()=>onClick()} {...rest}>
    <ButtonText disabled={disabled} cancel={cancel} weight={weight}>{label}</ButtonText>
  </Button>
  }