import React, { useState } from "react";
import RadioGroup from '@material-ui/core/RadioGroup';
import FlexView from "@atom/FlexView";
import Text from "@atom/Text";
import RadioButton from "@atom/RadioButton";
import Input, { SimpleInput as BaseSimpleInput } from "@atom/Input";
import { styled } from "@material-ui/styles";

const SimpleInput = styled(BaseSimpleInput)(({theme})=>({
  width: "50%",
  margin: "0 15px",
  background: "transparent",
  border: "none",
  borderBottom: `1px solid ${theme.common.white}`
}))

export default () => {
  const [value, setValue] = useState("")
  return <FlexView width={"100%"} direction={"row"} wrap={"wrap"}>
    <RadioGroup value={value} onChange={(event)=>setValue(event.target.value)}>
      <FlexView direction={"row"} width={"100%"} wrap={"wrap"}>
        <RadioButton value={"artificialIntelligence"} label={"Artificial Intelligence"} />
        <RadioButton value={"cyberSecurity"} label={"Cyber Security"} />
        <RadioButton value={"bigData"} label={"Big Data"} />
        <RadioButton value={"webDevelopmentFrontend"} label={"Web Development - Frontend"} />
        <RadioButton value={"machineLearning"} label={"Machine Learning"} />
        <RadioButton value={"webDevelopmentBackend"} label={"Web Development - Backend"} />
        <RadioButton value={"eRPOracle"} label={"ERP Oracle"} />
        <RadioButton value={"webDevelopmentFullStack"} label={"Web Development - Full-Stack"} />
        <RadioButton value={"eRPSAP"} label={"ERP SAP"} />
        <RadioButton value={"reactNative"} label={"React Native"} />
        <RadioButton value={"dataScience"} label={"Data Science"} />
        <RadioButton value={"userExperienceUX"} label={"User Experience - UX"} />
        <RadioButton value={"dataMining"} label={"Data Mining"} />
        <RadioButton value={"userInterfaceUI"} label={"User Interface - UI"} />
        <RadioButton value={"blockchain"} label={"Blockchain"} />
        <RadioButton value={"preSales"} label={"Pre-Sales"} />
        <RadioButton value={"network"} label={"Network"} />
        <RadioButton value={"crm"} label={"CRM"} />
        <RadioButton value={"communication"} label={"Communication"} />
        <FlexView className={"details input-other"} width={"50%"} direction={"row"} justify={"flex-start"}>
          <Text>{"Other:"}</Text>
            <SimpleInput width={"100%"} />
        </FlexView>
        <RadioButton value={"security"} label={"Security"} />
        <FlexView width={"50%"}/>
      </FlexView>
    </RadioGroup>
  </FlexView>
}