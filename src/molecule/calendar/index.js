import React, { useState } from 'react';
import Calendar from "react-datepicker";

import "react-datepicker/dist/react-datepicker.css";
import "./calendar.scss";

export default ({placeholder}) => {
  const [value, setValue] = useState("")
  // <div>
  return <Calendar
          placeholderText={placeholder || ""}
          selected={value}
          onChange={date =>setValue(date)}
          // value={value}
        />
  // </div>
}