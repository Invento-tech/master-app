import React, { useState } from "react";
import RadioGroup from '@material-ui/core/RadioGroup';
import FlexView from "@atom/FlexView";
import Text from "@atom/Text";
import RadioButton from "@atom/RadioButton";
import Input, { SimpleInput as BaseSimpleInput } from "@atom/Input";
import { styled } from "@material-ui/styles";

const SimpleInput = styled(BaseSimpleInput)(({theme})=>({
  width: "50%",
  margin: "0 15px",
  background: "transparent",
  border: "none",
  borderBottom: `1px solid ${theme.common.white}`
}))

export default () => {
  const [value, setValue] = useState("")
  return <FlexView width={"100%"} direction={"row"} wrap={"wrap"}>
    <RadioGroup value={value} onChange={(event)=>setValue(event.target.value)}>
      <FlexView direction={"row"} width={"100%"} wrap={"wrap"}>
        <RadioButton value={"administrator"} label={"Administrator"} />
        <RadioButton value={"head"} label={"Head"} />
        <RadioButton value={"analyst"} label={"Analyst"} />
        <RadioButton value={"manager"} label={"Manager"} />
        <RadioButton value={"architect"} label={"Architect"} />
        <RadioButton value={"programmer"} label={"Programmer"} />
        <RadioButton value={"consultant"} label={"Consultant"} />
        <RadioButton value={"specialist"} label={"Specialist"} />
        <RadioButton value={"CTOCIO"} label={"CTO/CIO"} />
        <RadioButton value={"supervisor"} label={"Supervisor"} />
        <RadioButton value={"designer"} label={"Designer"} />
        <RadioButton value={"technician"} label={"Technician"} />
        <RadioButton value={"developer"} label={"Developer"} />
        <RadioButton value={"trainer"} label={"Trainer"} />
        <RadioButton value={"director"} label={"Director"} />
        <RadioButton value={"freshgraduate"} label={"I'm Fresh - Never had a job"} />
        <RadioButton value={"engineer"} label={"Engineer"} />
        <FlexView className={"details input-other"} width={"50%"} direction={"row"} justify={"flex-start"}>
          <Text>{"Other:"}</Text>
            <SimpleInput width={"100%"} />
        </FlexView>
      </FlexView>
    </RadioGroup>
  </FlexView>
}