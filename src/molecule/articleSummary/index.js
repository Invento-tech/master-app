import React from "react"
import { styled } from "@material-ui/styles"
import Image from "@atom/Image"
import Text, { EnhancedText } from "@atom/Text"
import FlexView from "@atom/FlexView"
import { theme } from "@core/themes"

const Details = styled(Text)(({theme})=>({
  fontSize: theme.font.size.details,
  color: theme.font.color.gray,
  textAlign: "center"
}))

export default ({title,icon,details})=>(
  <FlexView width={"100%"} maxWidth={380} padding={20}>
    {icon && <Image src={icon} height={68} width={68}/>}
    {title && <EnhancedText size={theme.font.size.title} padding={"20px 0"} primary className={"title"} weight={700}>{title}</EnhancedText>}
    {details && <Details className={"details"}>{details}</Details>}
  </FlexView>
)