import React, {useState} from "react";
import FlexView from "@atom/FlexView";
import Text from "@atom/Text";
import Input from "@atom/Input";
import { styled } from "@material-ui/styles";
// import Button from "@atom/Button";
import BaseBorder from "@atom/Border";
import Select from "@atom/Select";
import nationalities from "@utils/nationalities";
import degrees from "@utils/degrees";
import SkillSet from "@molecule/skillSet";
import civilStatus from "@utils/civilStatus";
import EducationForm from "@molecule/educationForm";
import DocumentForm from "@compound/documentForm";
import ConfirmApplication from "@compound/confirmApplication";
import { history } from "@core/Router";
import { theme } from "@core/themes";
import Button from "@molecule/formButton";
import Calendar from "@molecule/calendar";

const Circle = styled(({status, left, ...rest})=><FlexView {...rest}/>)(({theme, status, left})=>({
  height: 48,
  width: 48,
  zIndex: 1,
  backgroundColor: status ?  theme.common.primary : "transparent",
  borderRadius: "50%",
  position: "absolute",
  left: left || "-35px",
  cursor: "pointer"
}))

const SmallCircle = styled(({status, ...rest})=><FlexView {...rest}/>)(({theme, status})=>({
  height: 12,
  width: 12,
  borderRadius: "500%",
  backgroundColor: status ? theme.font.color.dark : theme.font.color.light
}))

const Border = styled(BaseBorder)(({theme, left})=>({
  border: `1.5px solid ${theme.font.color.gray}`,
  minWidth: 185,
  position: "absolute",
  left: left || "0"
}))

const CircleRoot = styled(FlexView)({
  position: "relative"
})

const SwitchText = styled(({status, left, ...rest})=><Text {...rest} />)(({theme, status, left})=>({
  color: status ? theme.common.primary : theme.font.color.gray,
  width: 110,
  top: 55,
  position: "absolute",
  right: left ? "unset" : "-40px"
}))

const SwitchRoot = styled(FlexView)({
  flexFlow: "wrap"
})
const ButtonContainer = styled(FlexView)({
  cursor: "pointer"
})

const PersonalInfo = ({handleNext}) => (
  <>
  <FlexView justify={"space-evenly"} wrap={"wrap"} direction={"row"}>
    <Text className={"title"} size={theme.font.size.subTitle} maxWidth={680} weight={700} width={"87%"}>{"Personal Information"}</Text>
    <Text size={31} maxWidth={620} width={"87%"}></Text>
    <Input name={"firstName"} placeholder={"First Name*"} radius={12} width={"87%"} maxWidth={620} />
    <Input name={"lastName"} placeholder={"Last Name*"} radius={12} width={"87%"} maxWidth={620} />
    <Input placeholder={"Phone No. 1*"} radius={12} width={"87%"} maxWidth={620} />
    <Input placeholder={"Phone No. 2*"} radius={12} width={"87%"} maxWidth={620} />
    <Calendar placeholder={"Date of Birth*"}/>
    {/* <Input placeholder={"Date of Birth*"} radius={12} width={"87%"} maxWidth={620} /> */}
    <Select width={"92%"} placeholder={"Nationality*"} options={nationalities} maxWidth={652}/>
    {/* <Input placeholder={"Marital Status"} radius={12} width={"87%"} maxWidth={620} /> */}
    <Select width={"92%"} placeholder={"Marital Status"} options={civilStatus} radius={12} maxWidth={652}/>
    <Input placeholder={"LinkedIn Profile"} radius={12} width={"87%"} maxWidth={620} />
    {/* <Select placeholder={"Highest Level of Education*"} options={degrees} radius={12} maxWidth={652}/> */}
    <Input placeholder={"City"} radius={12} width={"87%"} maxWidth={620} />
    <FlexView maxWidth={620} width={"100%"} padding={22}/>

  </FlexView>
  <FlexView padding={"60px 0"}>
    <Button label={"Next"} onClick={()=>handleNext()}/>
  </FlexView>
  </>
)


const Education = ({handleCancel, handleNext}) => (
  <FlexView width={"95%"} justify={"space-evenly"} wrap={"wrap"} direction={"row"}>
    <FlexView width={"100%"}>
      <Text width={"100%"} className={"details centered"} size={theme.font.size.details} weight={"lighter"}>{"Here you tell us about your educational background including degrees, certificate, and majors"}</Text>
    </FlexView>
    <EducationForm handleCancel={handleCancel} handleNext={handleNext}/>
  </FlexView>
)

const Breadcrumb = ({page, setPage}) =>(
  <FlexView width={"100%"} direction={"row"} justify={"flex-start"}>
    <Text padding={"0 8px 0 0"} color={page >= 1 ? theme.common.primary : theme.font.color.normal}  size={13} onClick={()=>setPage(1)}>{"Personal Info > "}</Text>
    <Text padding={"0 8px 0 0"} color={page >= 2 ? theme.common.primary : theme.font.color.normal}  size={13} onClick={()=>setPage(2)}>{"Skills > "}</Text>
    <Text padding={"0 8px 0 0"} color={page >= 3 ? theme.common.primary : theme.font.color.normal}  size={13} onClick={()=>setPage(3)}>{"Education > "}</Text>
    <Text padding={"0 8px 0 0"} color={page >= 4 ? theme.common.primary : theme.font.color.normal}  size={13} onClick={()=>setPage(4)}>{"Documents"}</Text>
  </FlexView>
  
)

export default () => {
const [page, setPage] = useState(0)
return <FlexView width={"100%"} maxWidth={1366}>
        {
        page !== 0 && <>
          <FlexView padding={"36px 0"} width={"100%"}>
            <Text size={theme.font.size.subTitle} className={"details"}>{"Please fill the form in English"}</Text>
          </FlexView>
          <FlexView display={"none"} align={"flex-start"} className={"breadcrumb-text"} padding={"30px 0"} width={"95%"}>
            <Breadcrumb page={page} setPage={setPage}/>
          </FlexView>
          <SwitchRoot className={"breadcrumb-switcher"} padding= {"40px 0 80px 0"} width={700} >
            <CircleRoot direction={"row"} width={700}>
              <Circle status={page === 1} onClick={()=>setPage(1)}>
                <SmallCircle status={page === 1}/>
                <SwitchText left status={page === 1} align={"flex-start"} size={18}>{"Personal Info"}</SwitchText>
              </Circle>
              <Border radius={"0px"}/>
              <Circle left={175} status={page === 2} onClick={()=>setPage(2)}>
                <SmallCircle status={page === 2}/>
                <SwitchText  status={page === 2} align={"flex-end"} size={18}>{"Skills"}</SwitchText>
              </Circle>
              <Border left={210} radius={"0px"}/>
              <Circle left={385} status={page === 3} onClick={()=>setPage(3)}>
                <SmallCircle status={page === 3}/>
                <SwitchText  status={page === 3} align={"flex-end"} size={18}>{"Education"}</SwitchText>
              </Circle>            
              <Border left={420} radius={"0px"}/>
              <Circle left={595} status={page ===4} onClick={()=>setPage(4)}>
                <SmallCircle status={page ===4}/>
                <SwitchText  status={page === 4} align={"flex-end"} size={18}>{"Documents"}</SwitchText>
              </Circle>            
            </CircleRoot>
          </SwitchRoot>
        </>
        }
          {page === 0 && <ConfirmApplication handleNext={()=>setPage(1)}/>}
          {page === 1 && <PersonalInfo handleNext={()=>setPage(2)}/>}
          {page === 2 && <SkillSet handleCancel={()=>setPage(1)} handleNext={()=>setPage(3)}/>}
          {page === 3 && <Education handleCancel={()=>setPage(2)} handleNext={()=>setPage(4)}/>}
          {page === 4 && <DocumentForm handleCancel={()=>setPage(3)} handleNext={()=>history.push("/")}/>}
  </FlexView>
}