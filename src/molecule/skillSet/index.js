import React from "react"
import AreasOfExperience from "@molecule/areasOfExperience";
import Occupation from "@molecule/occupation";
import SalaryRange from "@molecule/salaryRange";
import { theme } from "@core/themes";
import FlexView from "@atom/FlexView";
import Text from "@atom/Text";
import Input from "@atom/Input";
// import Button from "@atom/Button";
import { styled } from "@material-ui/styles"
import Button from "@molecule/formButton";

const CancelButton = styled(Button)(({theme})=>({
  background: theme.common.dark,
  border: `1px solid ${theme.common.primary}`,
}))

const ButtonText = styled(({cancel, ...rest})=><Text {...rest} />)(({theme, cancel})=>({
  color: cancel ? theme.common.primary : theme.font.color.dark,
  fontSize: theme.font.size.buttonText,
}))

export default ({handleCancel, handleNext}) => (
  <FlexView justify={"space-evenly"} wrap={"wrap"} direction={"row"} width={"95%"}>
    <Text className={"details centered"} size={theme.font.size.details} weight={"lighter"}>{"Here you tell us about your work experience and your skills through your career"}</Text>
    <FlexView  padding={"20px 0"} width={"100%"}>
      <Text weight={700} size={theme.font.size.details} width={"100%"} className={"details"} align={"flex-start"} padding={"0px 0px 25px 0"}>{"Select areas of experience/knowledge:"}</Text>
      <AreasOfExperience/>
      <FlexView width={"100%"} direction={"row"} wrap={"wrap"} justify={"space-between"} padding={"50px 20px"}>
        <FlexView width={"95%"} maxWidth={550}>
          <Text size={theme.font.size.details} align={"left"} width={"100%"}>{"Current/Last Job Position"}</Text>
          <Input placeholder={"Your answer"} radius={12} width={"95%"}></Input>
        </FlexView>
        <FlexView width={"95%"} maxWidth={550}>
          <Text size={theme.font.size.details} align={"left"} width={"100%"}>{"Experience Years"}</Text>
          <Input placeholder={"Your answer"} radius={12} width={"95%"}></Input>
        </FlexView>        
      </FlexView>
      <FlexView  padding={"50px 0"} width={"100%"}>
        <Text weight={700} size={theme.font.size.details} width={"100%"} className={"title"} align={"flex-start"}>{"Select the best occupation that describes you:"}</Text>
      </FlexView>
      <Occupation/>
      <FlexView padding={"50px 0"} width={"100%"} align={"flex-start"}>
        <Text weight={700} size={theme.font.size.details} width={"95%"} className={"title"} align={"flex-start"}>{"Current/Last salary range:"}</Text>
        <SalaryRange/>
      </FlexView>
      <FlexView padding={"50px 0"}>
        <Text align={"center"} size={theme.font.size.details} weight={"lighter"}>{"To enter your skills, use # then the skill you have for example: #CRM #SharePoint #CCNA #CEH #CMA #PMP #PHP #SQL #.NET #JQuery #HTML #NTP #DNS #CISCO #SyberSecurity #AI #UI #UX #ERP #SAP #Oracle #DBA #IT #ICT #Backend #Frontend"}</Text>  
      </FlexView>    
      <FlexView padding={"40px 20px"} weight={700} width={"100%"} maxWidth={1366} justify={"flex-start"}>
        <Text width={"100%"} size={theme.font.size.details}>{"Using #yourSkill enter all the skills you have:"}</Text>
        <Input placeholder={"Enter a # with every skill you type"}/>
      </FlexView>
    </FlexView>
    <FlexView padding={"50px 0"} width={"100%"} direction={"row"} wrap={"wrap"}>
      <Button label={"Back"} margin={20} onClick={()=>handleCancel()} cancel/>
      <Button label={"Next"} margin={20} onClick={()=>handleNext()}/>    
    </FlexView>
  </FlexView>
)