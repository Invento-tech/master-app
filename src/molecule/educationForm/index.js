import React, { useState } from "react";
import RadioGroup from '@material-ui/core/RadioGroup';
import FlexView from "@atom/FlexView";
import Text from "@atom/Text";
import RadioButton from "@atom/RadioButton";
import Input, { SimpleInput as BaseSimpleInput } from "@atom/Input";
import { styled } from "@material-ui/styles";
// import Button from "@atom/Button";
import { theme } from "@core/themes";
import Button from "@molecule/formButton";

const SimpleInput = styled(BaseSimpleInput)(({theme})=>({
  width: "50%",
  margin: "0 15px",
  background: "transparent",
  border: "none",
  borderBottom: `1px solid ${theme.common.white}`
}))

const FreshGraduate = ()=>{
  const [value, setValue] = useState("")
  return <FlexView justify={"flex-start"} width={"100%"} direction={"row"} wrap={"wrap"}>
    <RadioGroup value={value} onChange={(event)=>setValue(event.target.value)}>
      <FlexView direction={"row"} width={"100%"} wrap={"wrap"}>
        <RadioButton value={"yes"} label={"Yes"} width={"100%"}/>
        <RadioButton value={"no"} label={"No"} width={"100%"}/>
      </FlexView>
    </RadioGroup>
  </FlexView>
}

const EducationLevel = ()=>{
  const [value, setValue] = useState("")
  return <FlexView width={"100%"} direction={"row"} wrap={"wrap"}>
    <RadioGroup value={value} onChange={(event)=>setValue(event.target.value)}>
      <FlexView direction={"row"} width={"100%"} wrap={"wrap"}>
        <RadioButton value={"High School"} label={"Secondary"} width={"100%"}/>
        <RadioButton value={"Diploma"} label={"Diploma"} width={"100%"}/>
        <RadioButton value={"Bachelor"} label={"Bachelor"} width={"100%"}/>
        <RadioButton value={"Master"} label={"Master"} width={"100%"}/>
        <RadioButton value={"Phd"} label={"PhD"} width={"100%"}/>
        {/* <FlexView className={"details input-other"} width={"100%"} direction={"row"} justify={"flex-start"}>
          <Text>{"Other:"}</Text>
            <SimpleInput width={"100%"} />
        </FlexView> */}
      </FlexView>
    </RadioGroup>
  </FlexView>
}

const Major = ()=>{
  const [value, setValue] = useState("")
  return <FlexView width={"100%"} direction={"row"} wrap={"wrap"}>
    <RadioGroup value={value} onChange={(event)=>setValue(event.target.value)}>
      <FlexView direction={"row"} width={"100%"} wrap={"wrap"}>
        <RadioButton value={"informationTechnology"} label={"Information Technology"} width={"100%"}/>
        <RadioButton value={"informationSystem"} label={"Information System"} width={"100%"}/>
        <RadioButton value={"computerScience"} label={"Computer Science"} width={"100%"}/>
        <RadioButton value={"softwareEngineering"} label={"Software Engineering"} width={"100%"}/>
        <RadioButton value={"systemEngineering"} label={"systemEngineering"} width={"100%"}/>
        <RadioButton value={"electricalEngineeringCommunication"} label={"Electrical Engineering & Communication"} width={"100%"}/>
        <RadioButton value={"digitalEngineering"} label={"Digital Engineering"} width={"100%"}/>
        <FlexView className={"details input-other"} width={"100%"} direction={"row"} justify={"flex-start"}>
          <Text>{"Other:"}</Text>
            <SimpleInput width={"100%"} />
        </FlexView>
      </FlexView>
    </RadioGroup>
  </FlexView>
}

export default ({handleCancel, handleNext}) => {
  return <FlexView width={"95%"} direction={"row"} wrap={"wrap"} padding={"20px 0"}>
    <Text className={"non-padded-details"} size={theme.font.size.details} weight={700} width={"100%"} padding={"30px 0px 10px 0"}>{"Fresh Graduate"}</Text>
    <FreshGraduate/>
    <Text className={"non-padded-details"} size={theme.font.size.details} weight={700} width={"100%"} padding={"30px 0px 10px 0"}>{"Select your educational level"}</Text>
    <EducationLevel/>
    <Text className={"non-padded-details"} size={theme.font.size.details} weight={700} width={"100%"} padding={"30px 0px 10px 0"}>{"Univeristy Major"}</Text>
    <Major/>
    <FlexView padding={"50px 0"} width={"100%"} direction={"row"} wrap={"wrap"}>
      <Button label={"Back"} margin={20} cancel onClick={()=>handleCancel()}/>  
      <Button label={"Next"} margin={20} onClick={()=>handleNext()}/>  
    </FlexView>

  </FlexView>
}