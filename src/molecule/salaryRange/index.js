import React, { useState } from "react";
import RadioGroup from '@material-ui/core/RadioGroup';
import FlexView from "@atom/FlexView";
import Text from "@atom/Text";
import RadioButton from "@atom/RadioButton";
import BaseInput, { SimpleInput as BaseSimpleInput } from "@atom/Input";
import { styled } from "@material-ui/styles";
import { theme } from "@core/themes";
import Select from "@atom/Select";
import currency from "@utils/currency";

const SimpleInput = styled(BaseSimpleInput)(({theme})=>({
  width: "50%",
  margin: "0 15px",
  background: "transparent",
  border: "none",
  borderBottom: `1px solid ${theme.common.white}`
}))

const Input = styled(BaseInput)({
  flex:1
})

export default () => {
  const [value, setValue] = useState("")
  return <FlexView width={"100%"} direction={"row"} wrap={"wrap"} maxWidth={600}>
          <Select placeholder={"Currency"} options={currency} radius={12} margin={0} maxWidth={150}/>
          <Input placeholder={"Salary"} radius={12}></Input>
    {/* <RadioGroup value={value} onChange={(event)=>setValue(event.target.value)}> */}
      {/* <FlexView direction={"row"} width={"100%"} wrap={"wrap"}> */}

        {/* <RadioButton value={"4k"} label={"4K to 8K S.R."} width={"100%"}/>
        <RadioButton value={"8k"} label={"8K to 12K S.R."} width={"100%"}/>
        <RadioButton value={"12k"} label={"12K to 16K S.R."} width={"100%"}/>
        <RadioButton value={"16k"} label={"16K to 20K S.R."} width={"100%"}/>
        <RadioButton value={"20k"} label={"20K to 24K S.R."} width={"100%"}/>
        <RadioButton value={"24k"} label={"24K to 28K S.R."} width={"100%"}/>
        <RadioButton value={"moreThan30k"} label={"More than 30K"} width={"100%"}/> */}
      {/* </FlexView> */}
    {/* </RadioGroup> */}
  </FlexView>
}