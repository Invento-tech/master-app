import React, {useState} from "react";
import BaseSelect from 'react-select';
import { styled } from "@material-ui/styles";
import { theme } from "@core/themes";

const Select = styled(BaseSelect)(({theme})=>({
  borderRadius: 12,
  padding: "7px 10px",
}))
export default ({options = [], width, margin, maxWidth, ...rest}) =>{
  const [value, setValue] = useState(null);
  return <Select
    onChange={selectedOption => {
      setValue(selectedOption);
    }}  
    styles={{
      container: styles => ({
        ...styles,
        width: width || "100%",
        background: theme.common.inputBackground,
        fontSize: theme.font.size.details,
        maxWidth: maxWidth || "unset",
        margin: margin || 8.5,
        color: theme.font.color.normal,
        fontFamily: theme.font.family,
        border: `1px solid ${theme.common.gray}`,
      }),
      control: styles => ({ 
        ...styles, 
        fontSize: theme.font.size.details,
        border: "none",
        background: theme.common.inputBackground, 
        borderRadius: 12,
        // border: `1px solid ${theme.common.gray}`,
        border: "none",
        boxShadow: "none",
        whiteSpace: "nowrap",
        textOverflow: "ellipsis",
        ':hover': {
          borderColor: theme.common.gray
        },
        color: theme.font.color.normal
      }),
      option: (styles)=> ({...styles, color: theme.font.color.light}),
      indicatorSeparator: styles =>({...styles, display: "none"}),
      singleValue: (styles) => ({...styles, color: theme.font.color.normal})
    }}
    value={value}
    options={options}
    isSearchable
    {...rest}
  />
  }