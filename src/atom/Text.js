import { styled } from '@material-ui/styles';
import { theme } from "@core/themes";
import React from "react";

let textColor = null;
export const Text = styled(({maxWidth, minWidth, whiteSpace, ...rest})=><span {...rest} />)(({theme, justify, direction, padding, size, color, width, align, weight, maxWidth, family, htmlFor, flex, minWidth, whiteSpace, ...rest}) =>({
  lineHeight: 1.5,
  width: width || "unset",
  maxWidth,
  fontSize: size || theme.font.size.details,
  textAlign: align || "left",
  color: color || theme.font.color.normal,
  padding: padding || 0,
  fontFamily: family || theme.font.family,
  fontWeight: weight || "normal",
  htmlFor,
  flex,
  minWidth,
  whiteSpace
  // ...rest
}));

export const EnhancedText = ({primary,white, gray, dark, lightBlue, rust, children, size, ...rest}) =>{
  if(primary){
    textColor = theme.common.primary
  }
  else if(dark) {
    textColor = theme.font.color.dark
  }
  else if(gray){
    textColor = theme.font.color.gray
  }
  else if(lightBlue){
    textColor = theme.font.color.lightBlue
  }
  else if(rust){
    textColor = theme.font.color.rust
  }
  else {
    textColor = theme.font.color.normal
  }

  return <Text color={textColor} size={size} {...rest}>
    {children}
  </Text>
}

export default Text;