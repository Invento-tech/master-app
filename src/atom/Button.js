import React from "react";
import { styled } from "@material-ui/styles";


const Button = styled('div')(({background, margin, theme, width, height, radius, textAlign, align, padding})=>({
  background: background || theme.common.primary,
  justifyContent: "center",
  alignItems: "center",
  alignSelf: align || "center",
  textAlign: textAlign || "center",
  display: "flex",
  margin,
  height,
  width,
  cursor: "pointer",
  borderRadius: radius || 5,
  padding: padding
}))

export default ({ children, ...rest }) => <Button {...rest}>{children}</Button>;
