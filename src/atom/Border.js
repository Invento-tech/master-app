import React from "react";
import { styled } from "@material-ui/styles"

export default styled('div')(({minWidth, padding, radius, theme})=>({
  height: 0,
  minWidth: minWidth || 200,
  border: `3px solid ${theme.common.primary}`,
  borderRadius: radius || 7
}))