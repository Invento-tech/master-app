import React from "react";
import { styled } from '@material-ui/styles';
import Label from "@atom/Text";
import FlexView from "@atom/FlexView";

export const Input = styled(({maxWidth, minWidth, ...rest})=><input {...rest} />)(({theme, size, justify, direction, padding, radius, maxWidth, width}) =>({
  width: "100%",
  fontSize: size || theme.font.size.details,
  color: theme.font.color.normal,
  outline: "none",
  border: "none",
  background: "transparent"
}));

const BaseTextArea = styled(({minWidth, ...rest})=><textarea {...rest}></textarea>)(({theme, width, height, size})=>({
  width: "100%",
  height: "100%",
  fontSize: size || theme.font.size.details,
  color: theme.font.color.normal,
  outline: "none",
  border: "none",
  background: "transparent",
  resize: "none"
}))
const Root = styled(FlexView)(({theme, width, height, maxWidth, padding, radius, minWidth, flex}) => ({
  width: width || "100%",
  height,
  flex,
  maxWidth,
  minWidth,
  border: `1px solid ${theme.common.gray}`,
  padding: padding || "15px",
  borderRadius: radius || 12,
  background: theme.common.inputBackground,
  margin: "8.5px"
}))

export default ({maxWidth, padding, width, flex, ...rest}) => (
  <Root maxWidth={maxWidth} padding={padding} flex={flex} width={width} {...rest}>
    <Input width={"100%"} {...rest} />
  </Root>
)

export const TextArea = ({children, width, height, flex, maxWidth, minWidth, placeholder, ...rest}) => (
  <Root maxWidth={maxWidth} flex={flex} width={width} height={height} minWidth={minWidth} {...rest}>
    <BaseTextArea width={width} height={height} placeholder={placeholder}></BaseTextArea>
  </Root>
)

export const SimpleInput = ({maxWidth, ...rest}) => (
    <Input maxWidth={maxWidth} {...rest} />
)