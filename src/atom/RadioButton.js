import React from "react";
import FlexView from "@atom/FlexView";
import Text from "@atom/Text";
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Radio from '@material-ui/core/Radio';
import { theme } from "@core/themes";

export default ({value, label, width, minWidth, justify, ...rest}) => (
  <FlexView justify={justify} direction={"row"} justify={"flex-start"} 
    width={ width || "50%"} 
    minWidth={minWidth || 423} className={"centered-view"}>
    <FormControlLabel value={value} control={<Radio className={"radio-btn details"}/>} label={<Text className={"details"} size={theme.font.size.details} width={"100%"}>{label}</Text>} />
  </FlexView>
)