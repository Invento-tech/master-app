import React from "react";
import { styled } from '@material-ui/styles';

const FlexView =  styled(({maxWidth, minWidth, maxHeight, minHeight, ...rest})=><div {...rest}/>)(({
  flex, 
  justify, 
  direction, 
  align, 
  padding, 
  height, 
  width, 
  maxWidth, 
  background, 
  minHeight,
  maxHeight,
  wrap,
  margin,
  position,
  minWidth,
  display
  }) =>({
  flexWrap:  wrap || "nowrap",
  display: display || "flex",
  flex: flex || "none",
  height: height,
  width: width,
  flexDirection: direction || "column",
  justifyContent: justify || "center",
  alignContent: align || "center",
  alignItems: align || "center",
  padding: padding || 0,
  maxWidth: maxWidth || "unset",
  minWidth: minWidth || "unset",
  maxHeight: maxHeight || "unset",
  minHeight: minHeight || "unset",
  position: position || "relative",
  background: background || "transparent",
  margin: margin || "unset",
  // border: `1px solid black`
}));

export const FlexViewImage = styled(FlexView)(({src, repeat, size, position,theme})=>({
  backgroundImage: `url(${src})`,
  backgroundRepeat: repeat || "noRepeact",
  backgroundSize: size || "cover",
  backgroundPosition: position || "center"
}))

export default FlexView