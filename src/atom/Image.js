import React from "react";
import { styled } from '@material-ui/styles';
import { LazyLoadImage } from 'react-lazy-load-image-component';

export default styled('img')(({src, width, height, background, position, repeat, padding}) =>({
  backgroundImage: `url(${src})`,
  width: width || "100%",
  height: height || "100%",
  background,
  backgroundPosition: position || "center",
  backgroundRepeat: repeat || "no-repeat",
  padding,
  backgroundImage: "none"
}));


export const LazyImage = ({children, src, width, height, padding }) => (
    <LazyLoadImage
      // alt={image.alt}
      height={height}
      src={src}
      width={width}>
      {children}
    </LazyLoadImage>
);