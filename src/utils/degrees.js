const degrees = [
"Less than high school",
"High school",
"Graduated from high school",
"Vocational course",
"Complete vocational course",
"Associate’s studies",
"Completed associate’s degree",
"Bachelor’s studies",
"Bachelor’s degree graduate",
"Graduate studies(Masters)",
"Post-graduate studies (Doctorate)",
"Doctoral degree graduate"]

export default degrees.map(item=>({value: item, label: item}))