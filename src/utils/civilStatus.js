const status = [
  "Single",
  "Married",
  "Divorced",
  "Widow/er"
]
  
  export default status.map(item=>({value: item, label: item}))