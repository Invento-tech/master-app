import React from "react";
import FlexView from "@atom/FlexView";
import Text from "@atom/Text";
import Form from "@molecule/form";
import { styled } from "@material-ui/styles";
import { theme } from "@core/themes";

const Title = styled(Text)(({theme})=>({
  padding: 12,
  width: "100%",
  background: theme.common.primary,
  color: theme.common.dark
}))

export default () => (
  <FlexView justify={"flex-start"} minHeight={"100vh"} height={"100%"} background={theme.background.normal} width={"100%"}>
    <FlexView padding={"80px 0 0 0"} width={"100%"}>
      <Title width={"100%"} size={theme.font.size.title} align={"center"} className={"title"}>{"Registration Application Form"}</Title>
    </FlexView>
    <Form/>
  </FlexView> 
)