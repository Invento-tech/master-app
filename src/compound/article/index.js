import React from "react";
import FlexView from "@atom/FlexView";
import ArticleSummary from "@molecule/articleSummary";
import opportunityIcon from "@asset/opportunity-icon.svg"
import workshopsIcon from "@asset/workshops-icon.svg"
import networkingIcon from "@asset/networking-icon.svg"

export default () => (
  <FlexView direction={"row"} padding={"60px 0"} wrap={"wrap"} align={"flex-start"}>
    <ArticleSummary
        icon={opportunityIcon}
        title={"Opportunities"}
        details={"You can get a better chance to be contacted for a new job opportunity Just Register Today"}
    />
    <ArticleSummary
        icon={workshopsIcon}
        title={"Workshops"}
        details={"You can attend some IT training and certifications that increases your chances for a better job"}
    />
    <ArticleSummary
        icon={networkingIcon}
        title={"Consultation"}
        details={"We are your trusted consultation. Our consultant is beyond C level"}
    />
</FlexView>
)
