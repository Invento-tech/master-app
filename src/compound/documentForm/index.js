import React from "react";
import FlexView from "@atom/FlexView";
import Text from "@atom/Text";
import FileUploader from "@molecule/FileUploader";
// import Button from "@atom/Button";
import { styled } from "@material-ui/styles"
import { theme } from "@core/themes";
import Button from "@molecule/formButton";

const CancelButton = styled(Button)(({theme})=>({
  background: theme.common.dark,
  border: `1px solid ${theme.common.primary}`,
}))

const ButtonText = styled(({cancel, ...rest})=><Text {...rest} />)(({theme, cancel})=>({
  color: cancel ? theme.common.primary : theme.font.color.dark,
  fontSize: theme.font.size.buttonText,
}))

export default ({handleCancel, handleNext})=>{
  return <FlexView  minHeight={800} maxWidth={700} justify={"flex-start"} width={"95%"}>
      <FlexView className={"padded-view"} padding={20} width={"100%"} align={"flex-start"}>
        <Text className={"details"} size={theme.font.size.details} weight={"lighter"}>{"Best way to rename your resume/CV file:"}</Text>
        <Text className={"details"} size={theme.font.size.details} weight={"lighter"}>{"First & Last name CV - IT Project Manager • ICT • Business Analyst"}</Text>
      </FlexView>
      <FlexView className={"padded-view"} padding={20} width={"100%"} align={"flex-start"}>
        <Text className={"details"} size={theme.font.size.details} weight={"lighter"}>{"My Example: "}</Text>
        <Text className={"details"} size={theme.font.size.details} weight={"lighter"}>{"Saud Shafai CV - Recruitment • Talent Acquisition • HR • Shared Services"}</Text>
      </FlexView>
      <FlexView  className={"padded-view"} width={"100%"} padding={20} align={"flex-start"}>
        <Text className={"details"} weight={700} size={theme.font.size.details} padding={"40px 0"}>{"Upload your Cover Letter"}</Text>
        <FileUploader/>
        <Text className={"details"} weight={700} size={theme.font.size.details} padding={"40px 0"}>{"Upload your Resume"}</Text>
        <FileUploader/>
      </FlexView>
      <FlexView padding={"50px 0"} width={"100%"} direction={"row"} wrap={"wrap"}>
        <Button label={"Back"} cancel margin={20} onClick={()=>handleCancel()}/>
        <Button label={"Submit"} margin={20} onClick={()=>handleNext()}/>
    </FlexView>      
    </FlexView>
}