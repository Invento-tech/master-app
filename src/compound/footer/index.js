import React from "react"
import FlexView from "@atom/FlexView"
import BaseText from "@atom/Text"
import Image from "@atom/Image"
import facebookLogo from "@asset/facebook-logo.svg";
import twitterLogo from "@asset/twitter.svg";
import linkedinLogo from "@asset/linkedin.svg";

import { styled } from "@material-ui/styles"

const Root = styled(FlexView)(({theme})=>({
  justifyContent: "space-between",
  background: theme.common.footer,
  minHeight: 88,
  height: "auto",
  width: "100%"
}))

const Text = styled(BaseText)(({theme})=>({
  color: theme.font.color.light
}))

const SocialMedia = styled(Image)(({theme})=>({
  cursor: "pointer"
}))

export default ()=>(
  <Root className={"footer"} direction={"row"} wrap={"wrap"}>
      <Text padding={"0 60px"} className={"details centered"}>{"© Copyright 2019 Master Technologies. All Rights Reserved."}</Text>
      <FlexView direction={"row"} padding={"0px 55px 0 55px"}>
        <SocialMedia src={facebookLogo} width={25} height={25} padding={25}/>
        <SocialMedia src={twitterLogo} width={25} height={25} padding={25}/>
        <SocialMedia src={linkedinLogo} width={25} height={25} padding={25}/>
      </FlexView>
  </Root>
)