import React from "react";
import FlexView from "@atom/FlexView";
import Text, {EnhancedText} from "@atom/Text";
import Input, { TextArea }  from "@atom/Input";
import { theme } from "@core/themes";
import BottomBorder from "@atom/Border";
import Button from "@molecule/formButton";

export default () => (
  <FlexView background={theme.common.dark} width={"100%"} minHeight={"100vh"} wrap={"wrap"} height={"100%"} className={"page-break"}>
    <FlexView maxWidth={1366} width={"100%"} height={"100%"}>
      <FlexView padding={20} className={"padded-view"} flex={1}>
        <FlexView>
          <Text className={"title"} size={theme.font.size.title}>{"Are you a company?"}</Text>
          <Text className={"title"} size={theme.font.size.title}>{"Interested to hire?"}</Text>
        </FlexView>
        <FlexView maxWidth={998} className={"padded-view"} padding={10}>
          <Text className={"details"} size={theme.font.size.details} align={"center"} weight={"lighter"}>{"Please fill the form with the right information and I will contact you."}</Text>
        </FlexView>      
        {/* <Text className={"title"} size={theme.font.size.title} padding={20}>{"Contact Us"}</Text> */}
        {/* <BottomBorder/> */}
      </FlexView>
      <FlexView padding={20} width={"95%"} flex={4} justify={"flex-start"}>
        <FlexView direction={"row"} width={"100%"} wrap={"wrap"}>
          <Input minWidth={223} className={"details"} placeholder={"Name"} flex={1}></Input>
          <Input minWidth={223} className={"details"} placeholder={"Email"} type={"email"} flex={1}></Input>
          <Input minWidth={223}  className={"details"} placeholder={"Number"} flex={1}></Input>
        </FlexView>
        <FlexView direction={"row"} wrap={"wrap"} width={"100%"}>
          <Input minWidth={223} className={"details"} placeholder={"Company Name"} flex={1} width={"95%"}></Input>
          <TextArea minWidth={223}  className={"details textview"} placeholder={"Message"} height={150} width={"96%"}/>
        </FlexView>
        <FlexView minWidth={223} padding={"20px"} className={"padded-view"}>
        <Button label={"Submit"} className={"non-padded-details"} onClick={()=>console.warn('submit')} weight={"bold"} />
          </FlexView>  

      </FlexView>
    </FlexView>
  
  </FlexView>
)
