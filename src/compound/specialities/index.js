import React from "react";
import FlexView from "@atom/FlexView";
import BorderBottom from "@atom/Border";
import BaseText, {EnhancedText } from "@atom/Text";
import { styled } from "@material-ui/styles";
import { theme } from "@core/themes";

const Text = styled(BaseText)(({theme})=>({
  fontSize: theme.font.size.details,
  color: theme.font.color.gray,
  textAlign: "center",
  borderColor: theme.font.color.gray,
  border: '1px solid',
  borderRadius: 75,
  height: 28,
  padding: 15,
  whiteSpace: 'nowrap',
  margin: 10,
  flex: 1,
}))

const PlainText = styled(BaseText)(({theme})=>({
  color: theme.font.color.gray,
  fontSize: theme.font.size.details
}))

export default () => (
  <FlexView width={"100%"} height={"100%"} minHeight={"100vh"} className={"page-break padded-view"} >
      <EnhancedText weight={700} className={"padded-view"} className={"title"} gray size={theme.font.size.title} padding={theme.font.padding.title}>{"Specialities"}</EnhancedText>
      <BorderBottom/>
      <FlexView maxWidth={800} direction={"row"} justify={"space-evenly"} width={"100%"} padding={"40px 0 0 0"} wrap={"wrap"}>
        <FlexView wrap={"wrap"} direction={"row"} width={"100%"}>
          <Text minWidth={170} className={"details"}>{"Blockchain"}</Text>        
          <Text minWidth={170} className={"details"}>{"Informatics"}</Text>
          <Text className={"details"}>{"IT Development"}</Text>
        </FlexView>
        <FlexView wrap={"wrap"} direction={"row"} width={"100%"}>
          <Text className={"details"}>{"Data Management"}</Text>
          <Text minWidth={170}  className={"details"}>{"Data Science"}</Text>
          <Text className={"details"}>{"IT Governance"}</Text>
        </FlexView>
        <FlexView wrap={"wrap"} direction={"row"} width={"100%"}>
          <Text minWidth={170} className={"details"}>{"Machine Learning"}</Text>
          <Text minWidth={170} className={"details"}>{"Cyber Security"}</Text>
          <Text className={"details"}>{"Digital Transformation"}</Text>
        </FlexView>
        <FlexView wrap={"wrap"} direction={"row"} width={"100%"}>
          <Text minWidth={170} className={"details"}>{"Big Data"}</Text>
          <Text minWidth={170} className={"details"}>{"Data Mining"}</Text>
          <Text minWidth={170} className={"details"}>{"Cloud"}</Text>
        </FlexView>
        <FlexView wrap={"wrap"} direction={"row"} width={"100%"}>
          <Text className={"details"} >{"Artificial Intelligence - AI"}</Text>
          <Text  className={"details"}>{"Geographic Information System - GIS"}</Text>
        </FlexView>
      </FlexView>
      <FlexView maxWidth={800} padding={20}>
        <PlainText align={"center"}>
          {"We are the master recruiters for most of the IT field in security, development, governance, programming and other related areas."}
        </PlainText>
      </FlexView>
  </FlexView>
)