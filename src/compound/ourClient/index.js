import React from "react";
import FlexView, {FlexViewImage} from "@atom/FlexView";
import BorderBottom from "@atom/Border";
import {EnhancedText } from "@atom/Text";
import governmentSector from "@asset/government-sector-image.svg";
import privateSector from "@asset/private-sector-image.svg";
import { styled } from "@material-ui/styles";
import { theme } from "@core/themes";

const CenterText = styled(EnhancedText)({
  textAlign: "center"
})
export default () => (
  <FlexView width={"100%"} height={"100%"} minHeight={"100vh"} className={"page-break padded-view"} >
      <EnhancedText weight={700} className={"padded-view"} className={"title"} gray size={theme.font.size.title} padding={theme.font.padding.title}>{"Our Clients"}</EnhancedText>
      <BorderBottom/>
      <FlexView direction={"row"} justify={"space-evenly"} width={"100%"} padding={"40px 0 0 0"} wrap={"wrap"}>
        <FlexViewImage 
          src={governmentSector}
          height={400}
          maxHeight={400}
          maxWidth={400}
          width={"100%"}
        >
          <CenterText className={"title"} primary size={theme.font.size.title} width={"70%"}>{"GOVERNMENT SECTOR"}</CenterText>
        </FlexViewImage>
        <FlexViewImage
          src={privateSector}
          height={400}
          maxHeight={400}
          maxWidth={400}
          width={"100%"}
        > 
          <CenterText className={"title"} primary size={theme.font.size.title} width={"70%"}>{"PRIVATE SECTOR"}</CenterText>
        </FlexViewImage>
      </FlexView>
  </FlexView>
)