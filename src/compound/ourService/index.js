import React from "react";
import Text, {EnhancedText } from "@atom/Text"
import FlexView from "@atom/FlexView"
import { styled } from "@material-ui/styles"
import BottomBorder from "@atom/Border"
import { theme } from "@core/themes";

export default () => (
  <FlexView width={"100%"} minHeight={"85vh"} maxHeight={"85vh"} className={"page-break page-break-vh"} height={"100%"} wrap={"wrap"} background={theme.background.normal}>
    <FlexView flex={1}>
      <FlexView direction={"row"} wrap={"wrap"} padding={theme.font.padding.title}>
          <EnhancedText className={"title"} size={theme.font.size.title} padding={"0 15px 0 0"} primary>{"I'm a Master"}</EnhancedText>
          <Text className={"title"} size={theme.font.size.title}>{"Recruiter in Other Fields"}</Text>
      </FlexView>
      <BottomBorder/>
    </FlexView>
    <FlexView flex={2} className={"padded-container"} direction={"row"} width={"100%"} justify={"space-evenly"} align={"self-start"} padding={"40px 20px"} wrap={"wrap"}>
        <FlexView maxWidth={385} className={"padded-view"} padding={20}>
          <EnhancedText size={theme.font.size.subTitle} primary align={"center"} width={"65%"} padding={"0 0 26px 0"} className={"title"}>{"Master Recruitment"}</EnhancedText>
          <Text className={"details"} align={"center"}>{"If you are working in the business industry, please apply via website"}</Text>
        </FlexView>
        <FlexView maxWidth={385} className={"padded-view"} padding={20}>
          <EnhancedText size={theme.font.size.subTitle} className={"title"} primary align={"center"} width={"65%"} padding={"0 0 26px 0"}>{"Master Bankers"}</EnhancedText>
          <Text className={"details"} align={"center"}>{"If you are working in the finance and banking industry,please apply via website"}</Text>
        </FlexView>
        <FlexView maxWidth={385} className={"padded-view"} padding={20}>
          <EnhancedText className={"title"} size={theme.font.size.subTitle} primary align={"center"} width={"65%"} padding={"0 0 26px 0"}>{"Master Technologies"}</EnhancedText>
          <Text className={"details"} align={"center"}>{"If you are working in the IT/ICT industry, please apply via website"}</Text>
        </FlexView>
    </FlexView>
  </FlexView>
)