import React from "react";
import FlexView from "@atom/FlexView"
import { styled } from '@material-ui/styles';
import Text from "@atom/Text";
// import Button from "@atom/Button";
import Logo from "@asset/master-tech-logo.png";
import Image from "@atom/Image";
import { history } from "@core/Router";
import { inject } from "mobx-react";
import { toJS } from "mobx";
import Button from "@molecule/formButton";

const Root = styled(FlexView)(({theme}) =>({
  maxWidth: "unset",
  width: "100vw",
  position: "fixed",
  background: theme.navBar.normal,
  boxShadow: theme.common.shadow,
  zIndex: 99,
  flexWrap: "wrap",
  minHeight: 80,
  height: "auto"
}))

const ButtonText = styled(Text)(({theme})=>({
  color: theme.font.color.dark,
  fontSize: theme.font.size.buttonText
}))

const LogoContainer = styled(Image)({
  height: 60,
  width: 150,
  cursor: "pointer",
  padding: "0 20px"
})

export default inject(({routing})=>({
  location: routing.location.pathname
}))(({location}) => (
  <Root className={"nav-bar"} justify={"space-between"} direction={"row"}>
    <FlexView padding={0} className={"logo"}>
      <LogoContainer src={Logo} onClick={()=>history.push("/")}/>
    </FlexView>
    { location !== "/application-form"  && <FlexView padding={"0 40px"}>
      {/* {<Button className={"apply-button"} width={180} height={60} radius={75} onClick={()=>history.push('/application-form')}>
        <ButtonText weight={"bold"}>{"Apply Now"}</ButtonText>
      </Button>
      } */}
      {
        <Button label={"Apply Now"} weight={"bold"}  className={"apply-button"} onClick={()=>history.push('/application-form')}/>
      }
    </FlexView>
    }
  </Root>
))