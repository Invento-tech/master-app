import React, { useState } from "react";
import FlexView from "@atom/FlexView";
import Text from "@atom/Text";
import RadioButton from "@atom/RadioButton";
import RadioGroup from '@material-ui/core/RadioGroup';
import { styled } from "@material-ui/styles";
import { theme } from "@core/themes";
import Button from "@molecule/formButton";


export default ({handleNext}) => {
  const [value, setValue] = useState("")
  const [applicant, setApplicant] = useState("")
  return <FlexView padding={"40px 20px"} width={"100%"}>
    <Text maxWidth={1150} width={"100%"} className={"details"} align={"center"} weight={"lighter"}>{"This is an application form for IT/ICT professionals who are looking for career opportunities. At Master Technologies, we empower your professional and career path."}</Text>
    <FlexView width={"100%"} padding={"43px 20px"}>
      <Text  weight={"lighter"} className={"non-padded-details"}>{"You will be filling these following 4 sections:"}</Text>
      <FlexView wrap={"wrap"} align={"baseline"}>
        <Text  weight={"lighter"} className={"non-padded-details"}>{"1. Your experience and skills"}</Text>
        <Text   weight={"lighter"} className={"non-padded-details"}>{"2. Educational background"}</Text>
        <Text   weight={"lighter"} className={"non-padded-details"}>{"3. Uploading your documents"}</Text>
        <Text  weight={"lighter"} className={"non-padded-details"}>{"4. Contact info and Preference"}</Text>
      </FlexView>
    </FlexView>
    {/* <Text  weight={"lighter"} className={"details"}>{"Please to fill the application, go ahead and select yes and click next."}</Text> */}
    <FlexView maxWidth={958} padding={10} width={"100%"}>
      <Text padding={"20px 0px"} weight={"bold"} className={"details"}>{"Are you IT Fresh/Professional Applicant?"}</Text>
      <FlexView maxWidth={611} justify={"flex-start"} width={"100%"} direction={"row"} wrap={"wrap"}>
        <RadioGroup value={applicant} onChange={(event)=>setApplicant(event.target.value)}>
          <FlexView width={"100%"} flex={1} wrap={"wrap"} className={"padded-view radio-flex-start"} minWidth={611} direction={"row"} justify={"space-evenly"}>
            <RadioButton  width={"unset"}  value={"itApplicant"} label={"No"} minWidth={"80px"} justify={"center"}/>
            <RadioButton width={"unset"}  value={"professional"} label={"Yes"} minWidth={"80px"} justify={"center"} />
          </FlexView>
        </RadioGroup>
      </FlexView>
    </FlexView>
    <FlexView maxWidth={958} padding={10} width={"100%"}>
      <Text padding={"20px 0px"} weight={"bold"} className={"details"}>{"Do you want to continue to fill Master Technologies application?"}</Text>
      <FlexView maxWidth={611} justify={"flex-start"} width={"100%"} direction={"row"} wrap={"wrap"}>
        <RadioGroup value={value} onChange={(event)=>setValue(event.target.value)}>
          <FlexView width={"100%"} flex={1} wrap={"wrap"} className={"padded-view radio-flex-start"} minWidth={611} direction={"row"} justify={"space-evenly"}>
            <RadioButton  width={"unset"}  value={"no"} label={"No"} minWidth={"80px"} justify={"center"}/>
            <RadioButton width={"unset"}  value={"yes"} label={"Yes"} minWidth={"80px"} justify={"center"} />
          </FlexView>
        </RadioGroup>
      </FlexView>
    </FlexView>
    <FlexView padding={"30px 0"} width={"100%"} direction={"row"} wrap={"wrap"}>
      {
      value === "yes" && applicant !== "" ? 
        <Button label={"Next"} onClick={()=>handleNext()} weight={"bold"}/>
      :
        <Button disabled label={"Next"} onClick={()=>handleNext()} weight={"bold"}/>
      }
    </FlexView>    
  </FlexView>
}