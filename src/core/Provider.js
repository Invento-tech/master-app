import React from 'react';
import {Provider} from 'mobx-react';
import {stores} from "./Persistor"
import Router from "./Router";
import ThemeProvider from '@core/themes'

export default ({children}) =>
  <Provider {...stores}>
    <Router>
      <ThemeProvider>
        {children}
      </ThemeProvider>
    </Router>
  </Provider>
