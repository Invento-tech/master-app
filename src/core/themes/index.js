
import { ThemeProvider } from '@material-ui/styles';
import { MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles"
import React from "react"

export const theme = {
  common: {
    primary: "#FDC001",
    shadow: "0px 3px 15px #00000019",
    fontColor: "#3A3A3A",
    footer: "#101319",
    white: "#ffffff",
    gray: "#707070",
    dark: "#13171E",
    inputBackground: "#252B2E",
    indicator: "#828885",
    radioButton: "#BCE0FD",
    disabled: "#82754c"
  },
  font: {
    color: {
      dark: "#00226E",
      light: "#7E7E7E",
      normal: "#FCFFF7",
      gray: "#707070",
      lightBlue: "#00226E",
      rust: "#E3AB96"
    },
    padding: {
      title: 10
    },
    size: {
      buttonText: 18, //32,
      title: 35, // 57
      subTitle: 28,
      details: 20, //32
      bigDetails: 25,
      header: 60, //72
      subHeader: 35, //41
    },
    family: "'Open Sans', sans-serif"
  },
  background: {
    normal: "#13171E"
  },
  navBar: {
    normal: "#FCFFF7"
  }
};

const muiTheme = createMuiTheme({
  custom: theme
})

export default props => (
  <MuiThemeProvider theme={muiTheme}>
    <ThemeProvider {...props} {...{ theme }} />
  </MuiThemeProvider>
)