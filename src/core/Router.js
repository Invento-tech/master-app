import React from 'react'
import { createBrowserHistory } from 'history';
import { RouterStore, syncHistoryWithStore } from 'mobx-react-router';
import { Router } from 'react-router';
import { create, persist } from 'mobx-persist';

const browserHistory = createBrowserHistory()

export const routerStore = new RouterStore()

export const history = syncHistoryWithStore(browserHistory, routerStore)

const hydrate = create({})
const schema = {}

export const store = persist(schema)(routerStore)

export default ({children}) => <Router history={history}>{children}</Router>
