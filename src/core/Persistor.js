import { create } from 'mobx-persist';
import { autorun} from "mobx";
import {store as routing} from "./Router";

// const hydrate = create({})

export const stores = {
  routing: routing
}
// hydrate('searchHotel', stores.searchHotel)
// hydrate('routing', stores.routing)
