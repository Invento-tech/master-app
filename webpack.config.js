const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
  entry: './index.js',
  output: {
    path: path.join(__dirname, '/build'),
    filename: 'checkly.js'
  },
  resolve: {
    alias: {
      "@template": path.resolve(__dirname, "src/template"),
      "@screen": path.resolve(__dirname, "src/screen"),
      "@core": path.resolve(__dirname, "src/core"),
      "@theme": path.resolve(__dirname, "src/theme"),
      "@atom": path.resolve(__dirname, "src/atom"),
      "@molecule": path.resolve(__dirname, "src/molecule"),
      "@compound": path.resolve(__dirname, "src/compound"),
      "@asset": path.resolve(__dirname, "src/asset"),
      "@store": path.resolve(__dirname, "src/store"),
      "@enhancer": path.resolve(__dirname, "src/core/enhancer"),
      "@utils": path.resolve(__dirname, "src/utils"),
    }
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: ['style-loader','css-loader']
      },      
      {
         test: /\.js$/,
         exclude: /node_modules/,
         use: {
          loader: 'babel-loader'
        }
      },
      {
        test: /\.(jpe?g|gif|png|svg)$/i,
        use: [
          {
            loader: 'file-loader',
          },
        ],
      },
      {
        test: /\.s[ac]ss$/i,
        use: [
          'style-loader',
          'css-loader',
          'sass-loader',
        ],
      },      
    ]
  },
  devServer: {
    historyApiFallback: true,
  },
  plugins: [
    new HtmlWebpackPlugin({
       template: './index.html',
       favicon: "./src/asset/favicon.ico",
    })
  ]
}