import React from 'react'
import Provider from "@core/Provider"
import AppScreen from "@screen"
import NavBar from "@compound/navBar"
import "./App.scss"

export default () => <Provider>
        <NavBar/>
        <AppScreen />
    </Provider>
